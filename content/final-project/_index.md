---
title: "Final Project"
---




### Initial Ideas

> #### 1.Untitled(Self-camera)

Seeing machines is an expanded definition of Photography, it is intended to encompass the ways that not only humans use technology to see the world, but the ways machines see the world for other machines. 

For the final project, I want to make a room of camera obscura, the audience could see the view outside of the camera obscura in the room. At the same time, there is a camera in the corner of the camera obscura, and a screen to show the livestream in the camera obscura. People can see themselves surveil themselves. The topic is who is looking at whom. 

For the first steps, because it is quite hard to build a room-size camera obscura, I'll start to build a large format camera. Then the camera could connect with a remote control system to take a selfie for an audience, and all the images would upload to Instagram automatically with a filter. 

> #### Resources 


- #### Installation Inspiration
[Bruce Nauman - Corridor](https://friendgenerator.club/?p=173)

![install](images/bruce1.jpeg)

[Lauri Astala - Twin Cities](https://www.lauriastala.com/video-installations/twin-cities/#:~:text=Twin%20Cities%20reflects%20on%20picturing,between%20reality%20and%20the%20cinematic.)

![install](images/lauti1.png)


- #### Build a Camera
[Build a large format camera](https://jongrepstad.com/building-a-large-format-camera/?utm_source=wechat_session&utm_medium=social&utm_oi=817399599349243904)

[Build a large format camera](https://www.youtube.com/watch?v=SKZOGgjh_F4)

- #### Camera Obscura 

[Javiervall Honrat-The Incised Shadow](https://javiervallhonrat.com/portfolio-items/trabajos-preparatorios/?portfolioCats=8)

[Portable Camera Obscura](http://bhamobscura.com/the-prototype-camera-obscura/)

[Richard Learoyd-Room-sized camara obscura](https://www.sfmoma.org/watch/richard-learoyd-takes-us-inside-his-giant-homemade-camera/)

[Abelardo Morell-Tent Camera](https://www.abelardomorell.net/tent-camera)

[Vera Lutter- room-sized cameras with pinhole](https://www.youtube.com/watch?v=dqyjeEFNWlE)


- #### Photogram

[Photogram](https://en.wikipedia.org/wiki/Photogram#:~:text=A%20photogram%20is%20a%20photographic,then%20exposing%20it%20to%20light.&text=The%20technique%20is%20sometimes%20called,in%20his%20exploration%20of%20rayographs.)

[Cyanotype Process](https://shootitwithfilm.com/how-to-make-cyanotype-prints/#:~:text=What%20is%20a%20Cyanotype%3F,own%20light%20sensitive%20photo%20paper.)
Cyanotype: UV light sensitive mixture from a few chemicals, making your own light sensitive photo paper. 

[Ultraviolet photography](https://en.wikipedia.org/wiki/Ultraviolet_photography)
Ultraviolet photography is a photographic process of recording images by using radiation from the ultraviolet (UV) spectrum only.


- #### Links from feedback
[Self Portrait by Random International](https://vimeo.com/12552176)

[3D Camera Obscura](http://www.3dcameraobscura.com/)

> #### To Build a Camera

> #### Laser-Cutting Pinhole camera

![filmholder](images/filmholder.jpg)

![result](images/result.jpg)

![result2](images/result2.jpg)

> #### 3D-Printing large format camera

![camera](images/camera.jpeg)

> ##### 3D Printing parts

- Print with any Filament

![3d](images/3d-1.png)


- Print with black Filament

![3d](images/3d-2.png)

> ##### Aluminum Rail:

![alum](images/alum.png)

> ##### Ground Glass

- [Tutorial to make a ground glass](https://www.youtube.com/watch?v=hxC48_sd6BM)

- Materials:

At least two pieces of ∼1/8″ thick glass, 4×5″ (slightly undersized is ok too – 3.9 x 4.9″);
600 Grit Silicon Carbide Powder;
Water in a cup;
An off-cut of wood. Approximately 2 x 4 x 2″;
Super Glue;
8×10″ piece of Cardboard;
Double sided Tape;
Emery Cloth or Sandpaper (bonus);
A tray bigger than your cardboard (if you want to contain the mess);

> ##### Bellows

![bellow](images/bellow.jpeg)

- [Tutorial to make bellow](http://salihonbashome.blogspot.com/2015/04/5-ribs-cut.html)

- Sizes:

Front Standard Opening: 96 x 96 mm

Rear Standard Opening: 145 x 145 mm

Recommended Stiffener Size: 12 mm with 2.5 mm gaps

Max Bellows Thickness: 0.4 mm

Recommended Bellows Draw: Minimum: 35 mm, Maximum: 300 mm


- Materials:

There are four things you will need to shop for. First, the inner layer of the bellows – this needs to be light-tight. Second, the stiffeners – these define the folds of the bellows. Third, the outer layer of the bellows – this can be (nearly) anything as long as it is thin and flexible. Finally, you will need a glue to hold everything together. For The Standard 4×5, make sure that the inner and outer layers combined are not thicker than 0.5 mm. 





#### Idea 2. Untitled(Censored Printer)

##### A CNC machine only outputs hot air, the censored texts and hidden history are printed on thermal paper. The thermal paper is exposed to the hot air blurred the shape of the text, and in the end, everything on the paper will disappear. Memory—individual as well as national—is fickle, sculpted by the exigencies of vanity and convenience, as well as the distortions created by political dictates.

##### Examples

 [Poem-Translation](https://thuthuthu.com/intersemiotic-interpretation/)

 [Lemon Juice Printer ](https://fabacademy.org/2021/labs/kamakura/assignments/mtm/)

##### Sketch 
![sketch1.png](images/Sketch1.png)


https://vimeo.com/12552176

