---
title: "About"
---


##### About Me

     Hi, I am Yujie, a first-year Master's student in Photography at Aalto. I'm from Chengdu, China, currently based in Helsinki. Graduated with a BE in Mechanical Engineering in 2019, I hope I could find connections with photography and Engineering study through Digital Fabrication.

     I work on self-reflective projects close to home, seeking to interrogate dominant historical narratives and individuality.
     The main topic in my artistic work is to explore the relationship between personal life experience and mainstream history storytelling. Photography is the main medium in my work, combined with video and text.
      
     I'm interested in invisible images. For me, invisible images are not only images produced by imaging systems or the digital metadata associated with those images, but also images of hidden histories.


![Just an image](profile.jpg)

[My Website](http://yujiezhou.me/)

[My instagram](https://www.instagram.com/yujie.jpg/) 