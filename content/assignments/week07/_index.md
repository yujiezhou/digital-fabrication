---
title: "Week 07 3D Scanning and Pringting"
---

> #### Assignment Requirement

- Test the design rules of the 3D printers at Aalto Fablab.
- Design an object that can not be made using subtractive manufacturing.
- 3D print the object you designed.
- 3D scan something and (optionally) print it.
- Document your process in a new page on your website.
- Submit a link to the page on your documentation website.

> #### 3D Scanning 

##### Scanner: Artect Leo

![image](images/leo.jpg)


##### The process to use Artect Leo: 

- Push button to start and stop 
- move around the object from every direction 
- all the surfaces to become green 
- go back to stage to see the scan 

![image](images/1.jpg)

![image](images/hands2.jpg)


##### What I scanned

- My Hands 

![image](images/hand.jpg)

- Me 

![image](images/person.jpg)



##### Conncet to Artec Studio 

- Artec Studio 15 : Import scan - Import the Leo- Look for the scanner - Using an IP address

 - Scanner: setting - network - Fablab IP - Enter in the screen - Connect 


![image](images/connect.jpg)

![image](images/2.jpg)

##### Process in Artec Studio 

- Editor - Eraser - Lasso selection - erase object 

- Tools - Fusion: Ooutlier removal: Apply

- Align - Autoalignment - Apply

- Tools - Fusion: Outlier removal: Apply - Global registration 

- Tools - FAST  FUSION - Apply 

- Tools - Postprocessing - Hole filling 

- Fix holes 

- Tools - Postprocessing - Mesh simplification (save in softwarre/harddrive )

- Texure - Export 

- File - export - meshs (png/stl )

![image](images/3.jpg)

![image](images/hands3.jpg)

![image](images/4.jpg)

##### Edit scan file in Blender
- [Tutorial](https://www.youtube.com/watch?v=-gJObxMTvcA)

- Import 3D-Scan as .obj-file

- Edit Mode 

![image](images/b-2.png)

Add modifier- Decimate

![image](images/b-1.png)

- Fill hole: Add modifier- remesh 

Still need to continue with Blender


> #### 3D Printing


##### What to print

I want to continue the camera by 3d Printing, started wiht the knob

![image](images/20.png)


##### Process of 3D printing 

![image](images/printer.jpg)

- Choose Material: PLA

- Navigator rotate - Material - press forward 

- Print  

- Programs - Utimaker Cura 

- Select printer: Ultimaker 2 Extended 

- Select material - Nozzle: 0.4mm

- Model: Thingiverse - Benchy 
   
- Save - SD Card transfer - Choose file - printing 


#### Design rules:

- The minimum distance between objects to make them do not stick together: 0.3mm
- Maxium angle do not need support system: 60 degree
- The minimum of wall thickness
- Infill percentage
- Printing direction
- Avoid sharp corner(won’t be printed)

#### Test Design Rules

Becauese 3D printing takes a lot of time, firstly, I used the infill percentage as 20% to see if I could save time by lower infill percentage.
It turned out not so successful:

![image](images/00.jpg)

![image](images/6.jpg)

Then I used the small knob as the testing object to test design rules. Changing the infill percentage to 60%

![image](images/7.jpg)

![image](images/8.jpg)

The top looks much better, but the hole looks worse

![image](images/10.jpg)

![image](images/11.jpg)

 Changing the infill percentage to 100%, actually did not see much difference from the one in 60%

![image](images/15.jpg)
