---
title: "Week 01 Introduction"
---
###  Week01: Introduction

> #### Assignment Requirement
>Create a GitLab account and use its CI tools to publish a website. Submit the following.
> - Link to your repository, e.g. https://gitlab.com/your-username/digital-fabrication/
> - Link to the published website, e.g. https://your-username.gitlab.io/digital-fabrication/
> - Add a picture and description about yourself. At least one paragraph of text.

>#### What I submitted 
> - <https://gitlab.com/yujiezhou/digital-fabrication>
>-  <https://yujiezhou.gitlab.io/digital-fabrication>
>
>![about](image_week01/about.png)

>##### Fab Academy
>
>- [Fab Academy 2022](https://fabacademy.org/2022/schedule.html)
>- [Fab Academy 2021 Assignments](https://fabacademy.org/2021/docs/assessment/index.html)   
>- [Fab Labs](https://www.fablabs.io)
>- [Fab Acamey People](https://fabacademy.org/2021/people.html)
>- [Aalto DigitalFabrication 2021](https://aaltofablab.gitlab.io/digital-fabrication-2021/)
              
