---
title: "Week 05 Computer-Controlled Cutting"
---

### Week 05: Computer-Controlled Cutting

> #### Assignment Requirement
>
- Characterize your lasercutter's focus, power, speed, rate and kerf.
- Cut something on the vinyl cutter.
- Design, lasercut a parametric press-fit kit that takes into account the kerf and can be assembled in multiple ways.
- Document all of the points above in a new page on your documentation website.reate a page for the project management assignment on your website.


> #### Machine Introduction

#### Vinyl Cutting 

- Machine - put the rolls in the white area - Select Sheet mode: Roll/Piece/Edge

![image](Images_VinylCutting/machine.jpg)

- Test the power : Origin-Test, if the test does not work well: Menu - Condition - Change force 

![image](Images_VinylCutting/control.jpg)
![image](Images_VinylCutting/test.jpg)

- Open Illustrator: Millimeters as Units, Color:RGB


- Text: Creat outlines

![image](Images_VinylCutting/outline_texts.PNG)

- Extension: Roland CutStudio - Move to Origin - Output the Paths

![image](Images_VinylCutting/move_to_origin.jpg)

- Outcome

![image](Images_VinylCutting/aalto.jpg)

- After Cut: Menu - Unsetup - Lift lever and move the material 

#### Heat-transfer vinyl 

- Insert Material: Matte Side up
![image](Images_VinylCutting/heat.jpg)

- Mirror Design

![image](Images_VinylCutting/mirror.PNG)

![image](Images_VinylCutting/outcome.jpg)

- Set Temperature: 130 degrees, Set Time: 10 seconds
- Pre heat fabric for 3 seconds
- Put the heat-transfer Vinyl on the fabric and then press
![image](Images_VinylCutting/heatoutcome.jpg)


Image: Object - Image Trace - Make and Expand
![image](Images_VinylCutting/eyes.jpg)


> - #### Laser Cutting 


- Fusion Pro 48  - Max Size: 1219 x 914 x 311 mm

 - Vector : Stroke: 0.01mm- No fill

 - Engrave: Disable Stroke, only fill.

 - Illustrator - Print - Choose Printer: Epilog Engraver - Ignore Artboards- Media Size: Custome; 
 ![image](Images_LaserCutting/artboard.jpg)

- Measure thickness before putting materials

- Boundary: shadow area could not been seen - choose Edit to move and rotate

- 'Selection' - ungroup 

- Parameter: Power . speed, frequency: 10w hz;  Unit - Percentage 

 ![image](Images_LaserCutting/parameter.jpg)

- Import Material settings : Engrave  or Vector 

- AutoFocus: Plunger - Quick Print  - After finishing: Wait 15-30 seconds

- Kerf: 0.1mm

 ![image](Images_LaserCutting/kerf01.jpg)
 ![image](Images_LaserCutting/kerf.jpg)
 ![image](Images_LaserCutting/kerf02.jpg)



### Assignment 

> #### VinylCutting

Follow the steps in Introduction part, I tired to cut texts.

 ![image](Images_VinylCutting/sleep2.jpg)

 ![image](Images_VinylCutting/sleep.jpg)



> #### LaserCutting

 #### Idea of Laser Cutting: Make a pinhole camera 

What is pinhole cameara:
Basically, a pinhole camera is a box with a tiny hole at one end and film or photographic paper at the other.

[Pinhole Camera Design Calculator](http://www.mrpinhole.com/calcpinh.php)

focal length = (pinhole diameter / 0.03679) ^ 2  , units are in mm.

eg: diameter:  0.3mm pinhole; focal length = (0.3mm / 0.03679) ^ 2 = 66.49mm 

Firtsty, I want to design a mini pinhole camear for test, for this focal length (24.4mm) the optimal pinhole diameter is 0.2mm.

Design in Fusion 360

![image](Images_Pinhole/1.png)

![image](Images_Pinhole/2.png)


![image](Images_Pinhole/3.png)


![image](Images_Pinhole/4.png)


![image](Images_Pinhole/5.png)


![image](Images_Pinhole/6.png)


![image](Images_Pinhole/7.png)


![image](Images_Pinhole/8.png)


![image](Images_Pinhole/9.png)


![image](Images_Pinhole/10.png)

![image](Images_Pinhole/11.png)


![image](Images_Pinhole/12.png)

> - #### Cutting Process ##

Import Media file for vetor

![image](Images_Pinhole/parameter.jpg)

Result: Too much power

Change: Search for parameters of material I am using
![image](Images_Pinhole/material.jpg)

Change parameters:

![image](Images_Pinhole/change.jpg)

Result:

![image](Images_Pinhole/pinhole.jpg)


![image](Images_Pinhole/pinhole2.jpg)

> - #### Move to Medium format pinhole camera 

Camera Case
![image](Images_Pinhole1/ai.png)

Film Holder
![image](Images_Pinhole1/ai2.png)

Problem: Used the same power but some parts could not be cut through, but some could.

![image](Images_Pinhole1/problem.jpg)

Solution: Take smaller size materials and take part of components. Make sure the materail on work table is flat.

Result: Film holder outside

![image](Images_Pinhole1/filmholder.jpg)

![image](Images_Pinhole1/result.jpg)

Result2: Film holder inside

![image](Images_Pinhole1/result2.jpg)

To be continued...

Files:
[Mini Pinhole](mini-pinhole.ai)
[Pinhole Case](PinholeCase.ai)
[Pinhole Film Holder](PinholeFilmHolder.ai)
