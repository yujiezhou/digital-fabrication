---
title: "Week 03 Project Management"
---

### Week03: Project Management

> #### Assignment Requirement
>
> - Create a page for the project management assignment on your website.
> - Describe how you created your website on that page.
> - Describe how you use Git and GitLab to manage files in your repository.

> #### Git and GitLab
>  [My GitLab Account](https://gitlab.com/yujiezhou/digital-fabrication)
> ##### Cheat Sheet 
> - `git status` (to see what changed)
> - `git add index.html` to upload file by file
> - `git add .` to upload all the files at once
> - `git commit -m ‘change you did’`
> - `git push` Upload to the Repository
> - `git pull` for downloading the last copy of the repository
>- `git merge` (to have a working copy
>- `git rm` in case you need to remove a tracked file
>- `git mv` move or rename a file ,a directory or a symlinkgit
>- `git log`check activities
>




> #### Set up Git 
> 
> [Tutorial](https://www.youtube.com/watch?v=sfFYqPGfUZU)
>
> [ADD SSH public Key](https://docs.gitlab.com/ee/ssh/).
> When you use SSH keys to authenticate to the GitLab remote server, you don’t need to supply your username and password each time.
> - generate SSH key
> - Print SSH public key
> ![sshkey](images_week3/ssh_key.png)
> - copy and paste it to the SSH Keys section of Gitlab Account
>
> Git Global Setup
> - ` git config --global user.name "yujiezhou"`
> - ` git config --global user.email "yujiezhou0813@gmail.com`
>
> Push an existing folder
> - `cd exsiting folder`
> - `git init`
> - `git remote add origin git@gitlab.com:yujiezhou/digital-fabrication.git`
> - `git remote -v`
> - `git add .`
> - `git commit -m 'initial commit'`
> - `git push -u origin master`
>
> Create a new repository
> - `git clone git@gitlab.com:yujiezhou/digital-fabrication.git`
>
> Contains .gitlab-ci.yml


> #### Using Hugo static website generator
>
> [Tutorail](https://aaltofi-my.sharepoint.com/personal/krisjanis_rijnieks_aalto_fi/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkrisjanis%5Frijnieks%5Faalto%5Ffi%2FDocuments%2FDigitalFabrication2022%2FFileShare%2FDFI%2F2022%2D01%2D27%2Fhugo%2Dand%2Dreadymade%2Dthemes%2Dbasics%2Emp4&parent=%2Fpersonal%2Fkrisjanis%5Frijnieks%5Faalto%5Ffi%2FDocuments%2FDigitalFabrication2022%2FFileShare%2FDFI%2F2022%2D01%2D27)
>
> [Theme I am using](https://themes.gohugo.io/themes/hugo-book/)
>
> Download-Install as git submodule
> Navigate to your hugo project root and run:
> - `git submodule add https://github.com/alex-shpak/hugo-book themes/hugo-book`
>
>Then run hugo (or set theme = "hugo-book"/theme: hugo-book in configuration file)
>
> Write content page using **Markdown**
> [Cheat Sheet](https://www.markdownguide.org/basic-syntax/)
>
> Add remote [website](https://yujiezhou.gitlab.io/digital-fabrication/) path in Config.toml 
> 
>
> 

> #### Image and Video Optimization
>
>  [ImageMagick](https://imagemagick.org/index.php)
>
>  - `brew install imagemagick`
>  - `brew install ghostscript`
>  - `magick mogrify -resize 1000x700 -quality 75 *.png`
>
>  [FFmpeg](https://www.ffmpeg.org/)
>
>  [HandBrake](https://handbrake.fr/)

> #### What I failed
>
> Did not use git pull after editing README.md
> ![fail1](images_week3/fail1.png)
>
> Edited elements of the theme in theme folder
> ![fail2](images_week3/fail2.png)
> 
> What to do next time: copy and paste to layout folder 
>
> Solution to reconnect 
>![solution1](images_week3/solution_1.png)
>![solution2](images_week3/solution_2.png)
>![solution3](images_week3/solution_3.png)
>

> #### What else I learned 
> - [Command line tutorial](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview)
> - [HTML Basic Tutorial](http://pub.fabcloud.io/tutorials/week01_principles_practices_project_management/html_basics.html)
> - [HTML Elements Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)
> - [CSS Tutorial](https://www.w3schools.com/css/default.asp)


