---
title: "Week 04 Computer-Aided Design"
---

### Week04: Computer-Aided Design

> #### Assignment Requirement
- Use CAD tools to design a possible final project.
- Explore one or more 2D vector graphics tools.
- Explore one or more 2D raster graphics tools.
- Explore one or more 3D modelling tools.
- Document your process with screenshots and descriptions.
- Create page containing the assignment documentation on your website.

 [Computer-aided desing tools](http://academy.cba.mit.edu/classes/computer_design/index.html)

> #### My Goal

This week the aim for me is to try a new one tool get familiar with 2D and 3D graphic tools and know how they work and to see which tool suit me well. I used *Tayasui Sketches*, *Free CAD*, *Auto CAD*, and *Fusion 360*.

> ## 2D Design

> ### Raster Based
In the digital medium, images are represented through a raster data structure based on a tessellation of the 2D plane into cells. These cells are pixels and represent units of color.

By combining many small pixels in a grid, a raster image is formed. This is both the way an image is displayed on a computer or TV screen (through an LCD matrix for example) and how it is stored in the computer memory (as a 2D array of values). Each pixel’s color is represented through numbers and the way these numbers map onto a specific color is dependent on the color mode (RGB, CMYK, grayscale…)

I am familiar with Photoshop, this week I downloaed Tayasui Sketches on my iPad to do sketches, and it is easy and clear to use.

> **Tayasui Sketches**

![image](Images/sketch4.PNG)


### Vector Based 
A vector graphic is an artwork made up of points, lines, and curves that are based upon mathematical equations, rather than solid colored square pixels.

This means no matter the size or how far zoomed in the image is, the lines, curves, and points remain smooth. The artwork will never have jagged lines or blurriness.

I used Illustrator before, this week I started to use FreeCAD, and there are some problems while using as documentation. I Followed the example from the tutorial and try to get familiar with the tool.

- ### Free CAD

First Question: How to navigate
![partdesing](Images_FreeCAD/Navigate.png)

Change to WorkBench to Part Design
![partdesing](Images_FreeCAD/partdesign.png)

 Make a new spreadsheet- working as parameters 
![IMAGES](Images_FreeCAD/spreedsheet.png)

 Cell Properties-Alias
![IMAGES](Images_FreeCAD/alias.png)

 Toggle active body
 ![IMAGES](Images_FreeCAD/toggle.png)

 Creat a new sketch
 ![IMAGES](Images_FreeCAD/sketch.png)


 PAD
  ![IMAGES](Images_FreeCAD/pad.png)

 Symmetric to plane 
 ![IMAGES](Images_FreeCAD/symmetric-2.png)

Clone 
![IMAGES](Images_FreeCAD/clone.png)


Change angle in data- rotate the object
![IMAGES](Images_FreeCAD/Rotate.png)

Create a new sketch on surface of LegA
![IMAGES](Images_FreeCAD/legA.png)


Creat a retangle - Fix the point of the object - Construction line - tow points together - make the line vertical, coinpoint constraint - symmetric constraint 
![IMAGES](Images_FreeCAD/afterfix.png)


 Create a pocket
 
 **Question:could not make the pocket 
![partdesing](Images_FreeCAD/Question2.png)

**Question: Cannot hide another leg, so cannot create edge for another leg
![partdesing](Images_FreeCAD/question3.png)

** Question: Cannot save
![partdesing](Images_FreeCAD/cannotsave.png)



### 3D Design

- ### Fusion 360
This is my first time to use Fusion 360, I Followed tutorials and use the example to know how it works.

 First step - Workspace: Design

 Sketch- Square, line, Line type: construction
![image](Images_Fusion/1.png)

Contraits-could be deleted to move 
![image](Images_Fusion/2.png)

Constaits- midpoint ,  coincidence
![image](Images_Fusion/3.png)

Specify dimension and finish sketch

Shitf+middle mouse botton to rotate
![image](Images_Fusion/4.png)

Modify- Change parameters
![image](Images_Fusion/5.png)

Extrude 
![image](Images_Fusion/6.png)

Change the distance with parameter 
![image](Images_Fusion/7.png)

Create a skech on surface and coincident 
![image](Images_Fusion/8.png)

Finish Sketch
![image](Images_Fusion/9.png)

Excude 
![image](Images_Fusion/10.png)

Edit feature - Double Click - Change Operation from Join to New body
![image](Images_Fusion/11.png)

Create-Mirror-Type:Bodies-choose panel
![image](Images_Fusion/12.png)

Skech3
![image](Images_Fusion/13.png)

Extrude - to object - new body
![image](Images_Fusion/14.png)

Mirror again
![image](Images_Fusion/15.png)

Hide bodies, create a new sketch
![image](Images_Fusion/16.png)

One Construction line, two lines, symmetry constrait
![image](Images_Fusion/17.png)

Extrude and Mirror
![image](Images_Fusion/18.png)

Mirror two bodiews 
![image](Images_Fusion/19.png)

Modify-Combine-Operation:Join
![image](Images_Fusion/20.png)

Modify- Combine- Operation:Cut-choose Keep Tools box
![image](Images_Fusion/21.png)

Make new sketch 
![image](Images_Fusion/22.png)

Extrude joint
![image](Images_Fusion/23.png)

Create- Pattern- Circular Pattern - Pattern type: Bodies
![image](Images_Fusion/24.png)

Modify-Combine-Join joints and walls- no Keep Tools- work to every wall
![image](Images_Fusion/25.png)

***Wrong point: Made joints to another body
What I did: Move body
![image](Images_Fusion/26.png)

Modify-Cut-edit feature of extrude 
![image](Images_Fusion/27.png)



- ### Practice to make one component of camera with Fusion 360

The components of camera I am aiming for (the upper-left corner one)
![image](Images/example.jpeg)

Set up sketch
![image](Images_Practice/1-1.png)

Mirror lines 
![image](Images_Practice/2.png)

Finish Sketch
![image](Images_Practice/3.png)

Extrude
![image](Images_Practice/4.png)

Sketch on the surface for another extrude
![image](Images_Practice/5.png)

Extrude to another direction 
![image](Images_Practice/6.png)
![image](Images_Practice/7.png)

Sketch on the bottom surface
![image](Images_Practice/8.png)

Extrude through all
![image](Images_Practice/9.png)


- ### Other tools I want to explore.


 #### [OpenSCAD](https://openscad.org/documentation.html)

 #### [Solvespace](https://solvespace.com/index.pl)

#### [libfive](https://libfive.com/)

#### Sketch Up 

#### Blender


Update:
Design files 

