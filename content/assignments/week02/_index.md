---
title: "Week 02 Principles and Practices"
---

###  Week02: Principles and Practices

> #### Assignment Requirement
>Create a sketch and describe an idea of your final project. Do the following.
> - Add a Final Project page on your website
> - Add a visual sketch of your final project idea to the Final Project page.
> - Describe your final project. The description should be at least one paragraph long.
> - Submit a link to the Final Project page on your website.

>   **What I submiited**
>
>![submit](images_week02/submit.png)