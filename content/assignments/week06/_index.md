---
title: "Week 06 Electronics Production"
---

### Week06: Electronics Production

> #### Assignment Requirement
>
- Characterize the design rules for the PCB production process at the Aalto Fablab.
- Make an in-circuit programmer. Create tool paths for the milling machine, mill it and stuff (solder components) the board.
- Test the board and debug if needed.


#### Using CopperCAMfor PCB millingwith SRM-20


[Recording Introduction](https://aaltofi-my.sharepoint.com/personal/krisjanis_rijnieks_aalto_fi/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkrisjanis%5Frijnieks%5Faalto%5Ffi%2FDocuments%2FDigitalFabrication2022%2FFileShare%2FDFI%2F2022%2D02%2D17%2FMDX%2D40%5FIntroduction%5FRaw%2Emkv&parent=%2Fpersonal%2Fkrisjanis%5Frijnieks%5Faalto%5Ffi%2FDocuments%2FDigitalFabrication2022%2FFileShare%2FDFI%2F2022%2D02%2D17)


> ### CopperCAM 

Download PCB Files in gitlab [programmers](https://gitlab.fabcloud.org/pub/programmers)

UPDI D11C- Gitlab- programmar UPDI D11C - download - open kicad pro - Open Copper CAM - Fiel open 

![image](images/updi.jpg)

Board Dimensions - margin: 2mm; thickness: 1.6mm; 

File- Origin- Origin: 0 , 0

![image](images/og.jpg)

Parameters - Tool Library

Tool 2 and Tool 3;
2 - engraving - diameter: 0.4 mm; 3 - contour - 1 mm;
Maximum depthn per pass : 0.1 mm;
Rotation : 9000;




![image](images/tb.jpg)

Parameters - Select Tools

![image](images/st.jpg)

Render - Output - Mill - Sequence: Engrave, Cutting outline - XY-zero point: White Cross - Z-zero point: Circuit surface - OK 

![image](images/op.jpg)

![image](images/file.jpg)

> ### SRM-20

Set up - Command Set: RML-1

![image](images/1.8.jpg)
![image](images/1.1.jpg)

Milling Tools

![image](images/1.0.jpg)

Attach 0.20-0.50 engraving milling bits to the head - move it close to board (around 1mm) - almost touch the surface - set Z origin 

![image](images/1.4.jpg)

Move the head to milling origin - set X and Y origin


![image](images/1.5.jpg)

Cut- Add - Choose engraving file - Output


![image](images/1.7.jpg)

Engraving:

![image](images/eg.jpg)


After engraving:

View-select milling tool : yellow 1mm  - remove engraving tool - intall new tool - Choose: Continue- move the board- move z axis - release tool - set z origin- move z up - (do not need to touch x and y ) - close down - choose cut- add- select T3- output- resume

![image](images/cut.jpg)


After Cutting:


View - Vacuum cleanner - move board - put back tools


![image](images/1.jpg)

#### Soldering 

Find components and put them in a box 


![image](images/2.1.jpg)

![image](images/2.0.jpg)


Turn on electricity - sponge - press + and - at the same time to turn on soldering - turn on  fumes extracter (RND lab)- Tool box - Clean copper board with alchhol 

![image](images/2.2.jpg)

check beep 


![image](images/2.3.jpg)


Soldering:

Can not feel my eyes.


![image](images/2.4.jpg)

> Process of testing!

##### Test 1: 

Question : Did not milling clearly, should not be connected 
![image](images/3.jpg)

Solution: Cut them apart 

![image](images/3-2.jpg)

##### Test 2:
Question : Connceted under the microcontroller 
![image](images/4.jpg)

Solution: Remove it and cut again 

![image](images/4-2.jpg)

##### Test 3:Partly working

![image](images/6.jpg)

It could work buy could not run hello program, then Matti found the resistor is not right.

Question: Used the wrong resistor

![image](images/5.jpg)

Solution: Change the resistor 

![image](images/5-2.jpg)




##### Test 4: 

Still partly working

Question: Copper heated out;
![image](images/8.jpg)

Solution: Connect again
![image](images/10.jpg)

#### Test 5: Working, testing board broken

![image](images/11.jpg)

Unfortunatley, the testing board seems broken, could not work on Matti's, too. Thanks Matti for helping me testing again and again.